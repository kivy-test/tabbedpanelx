'''
Created on 07.12.2019
@author: i
'''

from kivy.app import App
from kivy.clock import Clock
from kivy.properties import BooleanProperty  # @UnresolvedImport
from kivy.uix.behaviors.focus import FocusBehavior
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.scrollview import ScrollView
from kivy.uix.textinput import TextInput
from kivy.uix.togglebutton import ToggleButton


class Txt(TextInput, FocusBehavior):
    ''' text input '''

    def __init__(self, **kwargs):  # pylint: disable=super-init-not-called
        TextInput.__init__(self, **kwargs)

        self.multiline = True
        self.write_tab = False
        self.size_hint = (1, 1)

    def keyboard_on_key_down(self, window, keycode, text, modifiers):
        '''
        Overwrite tab handling of focusbehavior (in textinput) because of
        problems with modifiers - set vs. list.
        '''

        # handle obly tab
        if keycode[1] == 'tab':

            # here modifiers are still a list, not a set
            if ['shift'] == modifiers:
                # from FocusBehavior
                next_focus = self.get_focus_previous()  # pylint: disable=no-member

            else:
                # from FocusBehavior
                next_focus = self.get_focus_next()  # pylint: disable=no-member

            if next_focus:
                # from FocusBehavior
                self.focus = False  # pylint: disable=attribute-defined-outside-init
                next_focus.focus = True

            return True

        return TextInput.keyboard_on_key_down(self, window, keycode, text, modifiers)


class TB(ToggleButton):  # pylint: disable=too-many-ancestors, too-many-instance-attributes
    ''' toggle button '''

    def __init__(self, xtabs, key, **kwargs):
        ToggleButton.__init__(self, **kwargs)

        self.group = 'titles'
        self.size_hint = (None, 1)
        self._xtabs = xtabs
        self._key = key

        self.background_disabled_down = 'atlas://data/images/defaulttheme/tab_btn_pressed'
        self.background_disabled_normal = 'atlas://data/images/defaulttheme/tab_btn_disabled'
        self.background_down = 'atlas://data/images/defaulttheme/tab_btn_pressed'
        self.background_normal = 'atlas://data/images/defaulttheme/tab_btn'

        # self.bind(size = self.setter('text_size'))

        def set_container_focus(*_args):
            if self.parent is not None and self.parent.parent is not None:
                self.parent.parent.focus = True
                # print('set focus ' + str(self.parent.parent))

        def state_changed(_btn, _state):
            # print('state changed: ' + state)
            # set focus in next frame!
            Clock.schedule_once(set_container_focus, 0)

        self.bind(state = state_changed)

    def on_touch_down(self, touch):
        if self.collide_point(*touch.pos):
            FocusBehavior.ignored_touch.append(touch)
            print('touch down on button')
            # select button
            self._xtabs.select(self._key)

    def on_touch_up(self, touch):
        ''' Blocks any touch. '''
        if self.collide_point(*touch.pos) and not touch.is_mouse_scrolling:
            print('touch up on button')
            # return True

        return ToggleButton.on_touch_up(self, touch)

    def on_press(self):
        print('press on button')
        ToggleButton.on_press(self)


class SvContainer(ScrollView):
    ''' The scroll view. '''

    def __init__(self, **kwargs):
        ScrollView.__init__(self, **kwargs)
        self.do_scroll_y = False
        self.size_hint = (1, None)
        self.height = 40

    def on_touch_down(self, touch):
        if self.collide_point(*touch.pos):
            FocusBehavior.ignored_touch.append(touch)
            print('touch down on scroller')

        return ScrollView.on_touch_down(self, touch)

    def set_focus(self, *_args):
        ''' Sets the focus. '''
        print('set focus - container ' + str(_args))
        # from FocusBehavior
        self.focus = True  # pylint: disable=attribute-defined-outside-init


class BoxContainer(BoxLayout):
    ''' The container of the headers. '''

    def __init__(self, **kwargs):
        BoxLayout.__init__(self, **kwargs)
        self.orientation = 'horizontal'
        self.size_hint = (None, 1)

    def on_touch_up(self, touch):
        ''' Blocks any touch. '''
        if self.collide_point(*touch.pos) and not touch.is_mouse_scrolling:
            print('touch up on box')
            return True

        return BoxLayout.on_touch_up(self, touch)


class Container(SvContainer, FocusBehavior):  # pylint: disable=too-many-instance-attributes
    ''' The titles. '''

    def __init__(self, xtabs, **kwargs):  # pylint: disable=super-init-not-called
        SvContainer.__init__(self, **kwargs)
        self.pos_hint = {'y': 'top'}

        self._xtabs = xtabs
        self.box = BoxContainer()
        super(Container, self).add_widget(self.box)

        def focus_changed(_btn, _state):
            print('set container focus: ' + str(_state) + ' ' + str(_btn))

        self.bind(focus = focus_changed)

    def add_widget(self, widget, index = 0):
        ''' Blocks add_widget. '''
        raise 'Its not allowed to add any widget!'

    def xadd(self, btn):
        ''' Adds a header button. '''

        # if btn.texture_size[0] == 0:
        btn.texture_update()
        if btn.width != btn.texture_size[0] + 20:
            btn.width = btn.texture_size[0] + 20

        self.box.add_widget(btn)

        width = 0
        for child in self.box.children:
            width += child.width
        self.box.width = width

    def sizer(self, *_args):
        ''' Sizes the box. '''
        width = 0
        for child in self.box.children:
            width += child.width
        self.box.width = width
        print('box: ' + str(width) + ' in ' + str(self.width))

    def keyboard_on_key_down(self, window, keycode, text, modifiers):
        # print(str(keycode) + ' ' + str(text) + ' ' + str(modifiers))

        direction = 0
        if keycode[1] == 'right':
            direction = -1
        elif keycode[1] == 'left':
            direction = 1

        last = len(self.box.children)
        if last > 0 and direction != 0:
            next_button = -99

            for idx in range(0, last):
                btn = self.box.children[idx]
                if btn.state == 'down':
                    next_button = idx + direction
                    if next_button < 0:
                        next_button = last - 1
                    elif next_button >= last:
                        next_button = 0

            if next_button >= 0:
                # print('next button ' + str(next_button))
                self._xtabs.select(self._xtabs.keys[next_button])

            return True

        return FocusBehavior.keyboard_on_key_down(self, window, keycode, text, modifiers)


class XTab():
    ''' A tab to describe a tab and its content of a tabbed widget. '''

    def __init__(self, xtabs, key, text, content):
        self._xtabs = xtabs
        self._key = key
        self._text = text
        self._content = content
        self._button = None

    def get_button(self):
        ''' Returns the header of the tab. '''

        if self._button is None:
            self._button = TB(self._xtabs, self._key, text = self._text)

        return self._button

    def get_content(self):
        ''' Returns the content do display for the selected tab in the content area. '''

        return self._content


class XTabs(BoxLayout):
    ''' The toggles... '''
    focus = BooleanProperty(False)

    def __init__(self, **kwargs):
        super(XTabs, self).__init__(**kwargs)
        self.orientation = 'vertical'

        self._current = None
        self._tabs = {}
        self.keys = []

        # create headers' region
        self.titles = Container(self)
        self.add_widget(self.titles)

        # TODO: init focus handling
        # set focus
        # titles.focus = True
        def set_focus(*_args):
            print('xtabs: set focus ' + str(_args))

        self.bind(focus = set_focus)

        # read focus
        def read_focus(*_args):
            print('xtabs: read focus ' + str(_args))

        self.titles.bind(focus = read_focus)

    def add(self, key, text, content):
        ''' Adds a tab page. '''

        self._tabs[key] = tab = XTab(self, key, text, content)
        self.keys.insert(0, key)

        btn = tab.get_button()
        first = False
        if self._current is None:
            btn.state = 'down'
            self._current = key
            first = True

        # TODO: block add
        self.titles.xadd(btn)

        if first:
            self.add_widget(tab.get_content())

    def select(self, key):
        ''' Selects the button with the given key. '''

        if self._current is not None:
            tab = self._tabs[self._current]
            btn = tab.get_button()
            btn.state = 'normal'
            self.remove_widget(tab.get_content())
            # print('deselect ' + self.current)

        tab = self._tabs[key]
        btn = tab.get_button()
        btn.state = 'down'
        self._current = key
        self.add_widget(tab.get_content())
        # print('select ' + self.current)
        self.titles.scroll_to(btn)


class Btn(Button, FocusBehavior):
    ''' A focusable button. '''

    def __init__(self, **kwargs):  # pylint: disable=super-init-not-called
        Button.__init__(self, **kwargs)
        self.bind(focus = self.focus_changed)

    def focus_changed(self, *_args):
        ''' Focus listener. '''
        if self.focus:
            print('Btn has gotton focus')
            self.background_normal = 'atlas://data/images/defaulttheme/tab_btn_pressed'
        else:
            print('Btn has lost focus')
            self.background_normal = 'atlas://data/images/defaulttheme/tab_btn'

    def on_touch_down(self, touch):
        if self.collide_point(*touch.pos):
            FocusBehavior.ignored_touch.append(touch)
            if not self.focus:
                self.focus = True

        return Button.on_touch_down(self, touch)


class ToggleApp(App):  # pylint: disable=too-few-public-methods
    ''' Test toggles. '''

    def build(self):  # pylint: disable=no-self-use
        ''' Creates the content. '''

        container = BoxLayout(orientation = 'vertical')

        btns = BoxLayout(size_hint = (1, None), height = 30)
        btn1 = Btn(text = 'Tf 1')
        btns.add_widget(btn1)
        btn2 = Btn(text = 'XTabs 1')
        btns.add_widget(btn2)
        btn3 = Btn(text = 'XTabs 2')
        btns.add_widget(btn3)
        container.add_widget(btns)

        txt = Txt(text = 'test\ntest\n...\n\n\ntest\ntest test test', size_hint = (1, 0.1))
        container.add_widget(txt)

        xtabs = XTabs(size_hint = (1, 3))
        ti1 = Txt(text = ('tab1\ntest text test text test text test text test text test text'
                          +' test text test text test text test text test text'))
        xtabs.add('btn1', 'Button 1 - extra verlängert - extra verlängert', ti1)

        ti2 = Txt(text = ('tab2\ntest text test text test text test text test text test text'
                          +' test text test text test text test text test text'))
        xtabs.add('btn2', 'Button 2 - etwas verlängert', ti2)

        ti3 = Txt(text = ('tab3\ntest text test text test text test text test text test text'
                          +' test text test text test text test text test text'))
        xtabs.add('btn3', 'Button 3 - extra verlängert - extra verlängert', ti3)

        ti4 = Txt(text = ('tab4\ntest text test text test text test text test text test text'
                          +' test text test text test text test text test text'))
        xtabs.add('btn4', 'Button 4 - etwas verlängert', ti4)
        container.add_widget(xtabs)

        xtabs = XTabs()
        xtabs.add('btn1',
                  'Button 1 - extra verlängert - extra verlängert',
                  Txt(text = 'tab 1\ntest test test'))
        xtabs.add('btn2',
                  'Button 2',
                  Txt(text = 'tab 2\n\n22 22 22\n\ntest test test'))
        container.add_widget(xtabs)

        return container


ToggleApp().run()
